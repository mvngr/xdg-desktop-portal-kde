# translation of xdg-desktop-portal-kde.pot to Esperanto
# Copyright (C) 2017 Free Software Foundation, Inc.
# This file is distributed under the same license as the xdg-desktop-portal-kde package.
# Oliver Kellogg <okellogg@users.sourceforge.net>, 2023.
#
# Minuskloj: ĉ ĝ ĵ ĥ ŝ ŭ   Majuskloj: Ĉ Ĝ Ĵ Ĥ Ŝ Ŭ
#
msgid ""
msgstr ""
"Project-Id-Version: desktop files\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-04-06 02:04+0000\n"
"PO-Revision-Date: 2023-03-26 21:54+0100\n"
"Last-Translator: Oliver Kellogg <okellogg@users.sourceforge.net>\n"
"Language-Team: Esperanto <kde-i18n-doc@kde.org>\n"
"Language: eo\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: translate-po (https://github.com/zcribe/translate-po)\n"
"X-Poedit-Language: Esperanto\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Oliver Kellogg"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "okellogg@users.sourceforge.net"

#: src/accessdialog.cpp:22
#, kde-format
msgid "Request device access"
msgstr "Peti aparatan aliron"

#: src/appchooser.cpp:92
#, kde-format
msgctxt "count of files to open"
msgid "%1 files"
msgstr "%1 dosieroj"

#: src/appchooserdialog.cpp:39
#, kde-format
msgctxt "@title:window"
msgid "Choose Application"
msgstr "Elekti Aplikon"

#: src/appchooserdialog.cpp:42
#, kde-kuit-format
msgctxt "@info"
msgid "Choose an application to open <filename>%1</filename>"
msgstr "Elektu aplikaĵon por malfermi <filename>%1</filename>"

#: src/AppChooserDialog.qml:31
#, kde-format
msgctxt "@option:check %1 is description of a file type, like 'PNG image'"
msgid "Always open %1 files with the chosen app"
msgstr "Ĉiam malfermi %1 dosierojn kun la elektita programo"

#: src/AppChooserDialog.qml:64
#, kde-format
msgid "Show All Installed Applications"
msgstr "Montri Ĉiujn Instalitajn Aplikojn"

#: src/AppChooserDialog.qml:169
#, kde-format
msgid "Default app for this file type"
msgstr "Defaŭlta aplikaĵo por ĉi tiu dosiertipo"

#: src/AppChooserDialog.qml:188
#, kde-format
msgid "No matches"
msgstr "Neniuj kongruoj"

#: src/AppChooserDialog.qml:188
#, kde-kuit-format
msgctxt "@info"
msgid "No installed applications can open <filename>%1</filename>"
msgstr "Neniuj instalitaj aplikaĵoj povas malfermi <filename>%1</filename>"

#: src/AppChooserDialog.qml:192
#, kde-format
msgctxt "Find some more apps that can open this content using the Discover app"
msgid "Find More in Discover"
msgstr "Trovi Pli en Discover"

#: src/AppChooserDialog.qml:207
#, kde-kuit-format
msgctxt "@info"
msgid "Don't see the right app? Find more in <link>Discover</link>."
msgstr "Ĉu vi ne vidas la ĝustan apon? Trovu pli en <link>Discover</link>."

#: src/background.cpp:104
#, kde-format
msgid "Background activity"
msgstr "Fona agado"

#: src/background.cpp:105
#, kde-format
msgid "%1 is running in the background."
msgstr "%1 funkcias en la fono."

#: src/background.cpp:106
#, kde-format
msgid "Find out more"
msgstr "Ekscii pli"

#: src/background.cpp:117
#, kde-format
msgid "%1 is running in the background"
msgstr "%1 funkcias en la fono"

#: src/background.cpp:119
#, kde-format
msgid ""
"This might be for a legitimate reason, but the application has not provided "
"one.\n"
"\n"
"Note that forcing an application to quit might cause data loss."
msgstr ""
"Ĉi tio eble estas pro legitima kialo, sed la aplikaĵo ne disponigis tian.\n"
"\n"
"Rimarku, ke devigi aplikaĵon ĉesi povus kaŭzi datumperdon."

#: src/background.cpp:122
#, kde-format
msgid "Force quit"
msgstr "Devigi ĉeson"

#: src/background.cpp:123
#, kde-format
msgid "Allow"
msgstr "Permesi"

#: src/dynamiclauncher.cpp:76
#, kde-format
msgctxt "@title"
msgid "Add Web Application…"
msgstr "Aldoni TTT-aplikaĵon…"

#: src/dynamiclauncher.cpp:81
#, kde-format
msgctxt "@title"
msgid "Add Application…"
msgstr "Aldoni Aplikaĵon…"

#: src/DynamicLauncherDialog.qml:72
#, kde-format
msgctxt "@label name of a launcher/application"
msgid "Name"
msgstr "Nomo"

#: src/DynamicLauncherDialog.qml:91
#, kde-format
msgctxt "@action edit launcher name/icon"
msgid "Edit Info…"
msgstr "Redakti informojn…"

#: src/DynamicLauncherDialog.qml:97
#, kde-format
msgctxt "@action accept dialog and create launcher"
msgid "Accept"
msgstr "Akcepti"

#: src/DynamicLauncherDialog.qml:102
#, kde-format
msgctxt "@action"
msgid "Cancel"
msgstr "Nuligi"

#: src/filechooser.cpp:365
#, kde-format
msgid "Open"
msgstr "Malfermi"

#: src/kirigami-filepicker/api/mobilefiledialog.cpp:100
#, kde-format
msgid "Select Folder"
msgstr "Elekti Dosierujon"

#: src/kirigami-filepicker/api/mobilefiledialog.cpp:103
#, kde-format
msgid "Open File"
msgstr "Malfermi Dosieron"

#: src/kirigami-filepicker/api/mobilefiledialog.cpp:105
#, kde-format
msgid "Save File"
msgstr "Konservi Dosieron"

#: src/kirigami-filepicker/declarative/CreateDirectorySheet.qml:17
#, kde-format
msgid "Create New Folder"
msgstr "Krei Novan Dosierujon"

#: src/kirigami-filepicker/declarative/CreateDirectorySheet.qml:26
#, kde-format
msgid "Create new folder in %1"
msgstr "Krei novan dosierujon en %1"

#: src/kirigami-filepicker/declarative/CreateDirectorySheet.qml:33
#, kde-format
msgid "Folder name"
msgstr "Nomo de dosierujo"

#: src/kirigami-filepicker/declarative/CreateDirectorySheet.qml:41
#, kde-format
msgid "OK"
msgstr "bone"

#: src/kirigami-filepicker/declarative/CreateDirectorySheet.qml:52
#, kde-format
msgid "Cancel"
msgstr "Nuligi"

#: src/kirigami-filepicker/declarative/FilePicker.qml:101
#, kde-format
msgid "File name"
msgstr "Dosiernomo"

#: src/kirigami-filepicker/declarative/FilePicker.qml:116
#, kde-format
msgid "Select"
msgstr "Elekti"

#: src/kirigami-filepicker/declarative/FilePickerWindow.qml:57
#, kde-format
msgid "Create Folder"
msgstr "Krei Dosierujon"

#: src/kirigami-filepicker/declarative/FilePickerWindow.qml:66
#, kde-format
msgid "Filter Filetype"
msgstr "Filtri Dosiertipon"

#: src/kirigami-filepicker/declarative/FilePickerWindow.qml:71
#, kde-format
msgid "Show Hidden Files"
msgstr "Montri Kaŝitajn dosierojn"

#: src/outputsmodel.cpp:18
#, kde-format
msgid "Full Workspace"
msgstr "Plena Laborspaco"

#: src/outputsmodel.cpp:23
#, kde-format
msgid "New Virtual Output"
msgstr "Nova Virtuala Eligo"

#: src/outputsmodel.cpp:27
#, kde-format
msgid "Rectangular Region"
msgstr "Rektangula Regiono"

#: src/outputsmodel.cpp:34
#, kde-format
msgid "Laptop screen"
msgstr "Ekrano de tekkomputilo"

#: src/region-select/RegionSelectOverlay.qml:142
#, kde-format
msgid "Start streaming:"
msgstr "Komenci streaming:"

#: src/region-select/RegionSelectOverlay.qml:146
#, kde-format
msgid "Clear selection:"
msgstr "Nuligi elekton:"

#: src/region-select/RegionSelectOverlay.qml:150
#: src/region-select/RegionSelectOverlay.qml:193
#, kde-format
msgid "Cancel:"
msgstr "Nuligi:"

#: src/region-select/RegionSelectOverlay.qml:156
#, kde-format
msgctxt "Mouse action"
msgid "Release left-click"
msgstr "Malteni maldekstran klakon"

#: src/region-select/RegionSelectOverlay.qml:160
#, kde-format
msgctxt "Mouse action"
msgid "Right-click"
msgstr "Dekstra-alklako"

#: src/region-select/RegionSelectOverlay.qml:164
#: src/region-select/RegionSelectOverlay.qml:203
#, kde-format
msgctxt "Keyboard action"
msgid "Escape"
msgstr "Eskapo"

#: src/region-select/RegionSelectOverlay.qml:189
#, kde-format
msgid "Create selection:"
msgstr "Krei elekton:"

#: src/region-select/RegionSelectOverlay.qml:199
#, kde-format
msgctxt "Mouse action"
msgid "Left-click and drag"
msgstr "Maldekstre alklaki kaj treni"

#: src/remotedesktop.cpp:121
#, kde-format
msgctxt "title of notification about input systems taken over"
msgid "Remote control session started"
msgstr "Seanco de teleregado komenciĝis"

#: src/remotedesktopdialog.cpp:23
#, kde-format
msgctxt "Title of the dialog that requests remote input privileges"
msgid "Remote control requested"
msgstr "Teleregilo petita"

#: src/remotedesktopdialog.cpp:32
#, kde-format
msgctxt "Unordered list with privileges granted to an external process"
msgid "Requested access to:\n"
msgstr "Petis aliron al:\n"

#: src/remotedesktopdialog.cpp:34
#, kde-format
msgctxt ""
"Unordered list with privileges granted to an external process, included the "
"app's name"
msgid "%1 requested access to remotely control:\n"
msgstr "%1 petis aliron al fora kontrolo:\n"

#: src/remotedesktopdialog.cpp:37
#, kde-format
msgctxt "Will allow the app to see what's on the outputs, in markdown"
msgid " - Screens\n"
msgstr "- Ekranoj\n"

#: src/remotedesktopdialog.cpp:40
#, kde-format
msgctxt "Will allow the app to send input events, in markdown"
msgid " - Input devices\n"
msgstr "- Enigaparatoj\n"

#: src/RemoteDesktopDialog.qml:26 src/ScreenChooserDialog.qml:135
#: src/UserInfoDialog.qml:41
#, kde-format
msgid "Share"
msgstr "Kunhavigi"

#: src/screencast.cpp:321
#, kde-format
msgctxt "Do not disturb mode is enabled because..."
msgid "Screen sharing in progress"
msgstr "Ekrandivido en progreso"

#: src/screencastwidget.cpp:24
#, kde-format
msgid ""
"Laptop screen\n"
"Model: %1"
msgstr ""
"Portebla ekrano\n"
"Modelo: %1"

#: src/screencastwidget.cpp:27 src/screencastwidget.cpp:30
#, kde-format
msgid ""
"Manufacturer: %1\n"
"Model: %2"
msgstr ""
"Fabrikisto: %1\n"
"Modelo: %2"

#: src/screenchooserdialog.cpp:142
#, kde-format
msgid "Screen Sharing"
msgstr "Ekrana Kunhavigo"

#: src/screenchooserdialog.cpp:172
#, kde-format
msgid "Choose what to share with the requesting application:"
msgstr "Elektu kion dividi kun la petanta aplikaĵo:"

#: src/screenchooserdialog.cpp:174
#, kde-format
msgid "Choose what to share with %1:"
msgstr "Elektu kion dividi kun %1:"

#: src/screenchooserdialog.cpp:182
#, kde-format
msgid "Share this screen with the requesting application?"
msgstr "Ĉu dividi ĉi tiun ekranon kun la petanta aplikaĵo?"

#: src/screenchooserdialog.cpp:184
#, kde-format
msgid "Share this screen with %1?"
msgstr "Ĉu dividi ĉi tiun ekranon kun %1?"

#: src/screenchooserdialog.cpp:189
#, kde-format
msgid "Choose screens to share with the requesting application:"
msgstr "Elektu ekranojn por kunhavigi kun la petanta aplikaĵo:"

#: src/screenchooserdialog.cpp:191
#, kde-format
msgid "Choose screens to share with %1:"
msgstr "Elektu ekranojn por kunhavigi kun %1:"

#: src/screenchooserdialog.cpp:195
#, kde-format
msgid "Choose which screen to share with the requesting application:"
msgstr "Elektu la ekranon por dividi kun la petanta aplikaĵo:"

#: src/screenchooserdialog.cpp:197
#, kde-format
msgid "Choose which screen to share with %1:"
msgstr "Elektu kiun ekranon kunhavigi kun %1:"

#: src/screenchooserdialog.cpp:207
#, kde-format
msgid "Share this window with the requesting application?"
msgstr "Ĉu dividi ĉi tiun fenestron kun la petanta aplikaĵo?"

#: src/screenchooserdialog.cpp:209
#, kde-format
msgid "Share this window with %1?"
msgstr "Ĉu dividi ĉi tiun fenestron kun %1?"

#: src/screenchooserdialog.cpp:214
#, kde-format
msgid "Choose windows to share with the requesting application:"
msgstr "Elektu fenestrojn por kunhavigi kun la petanta aplikaĵo:"

#: src/screenchooserdialog.cpp:216
#, kde-format
msgid "Choose windows to share with %1:"
msgstr "Elektu fenestrojn por kunhavigi kun %1:"

#: src/screenchooserdialog.cpp:220
#, kde-format
msgid "Choose which window to share with the requesting application:"
msgstr "Elektu kiun fenestron por dividi kun la petanta aplikaĵo:"

#: src/screenchooserdialog.cpp:222
#, kde-format
msgid "Choose which window to share with %1:"
msgstr "Elektu kiun fenestron kunhavigi kun %1:"

#: src/ScreenChooserDialog.qml:36
#, kde-format
msgid "Screens"
msgstr "Ekranoj"

#: src/ScreenChooserDialog.qml:39
#, kde-format
msgid "Windows"
msgstr "Fenestroj"

#: src/ScreenChooserDialog.qml:129
#, kde-format
msgid "Allow restoring on future sessions"
msgstr "Permesi restarigi en estontaj seancoj"

#: src/screenshotdialog.cpp:115
#, kde-format
msgid "Full Screen"
msgstr "Plenekrane"

#: src/screenshotdialog.cpp:116
#, kde-format
msgid "Current Screen"
msgstr "Nuna Ekrano"

#: src/screenshotdialog.cpp:117
#, kde-format
msgid "Active Window"
msgstr "Aktiva Fenestro"

#: src/ScreenshotDialog.qml:23
#, kde-format
msgid "Request Screenshot"
msgstr "Peti Ekrankopion"

#: src/ScreenshotDialog.qml:30
#, kde-format
msgid "Capture Mode"
msgstr "Kapta Reĝimo"

#: src/ScreenshotDialog.qml:34
#, kde-format
msgid "Area:"
msgstr "Areo:"

#: src/ScreenshotDialog.qml:39
#, kde-format
msgid "Delay:"
msgstr "Prokrasto:"

#: src/ScreenshotDialog.qml:44
#, kde-format
msgid "%1 second"
msgid_plural "%1 seconds"
msgstr[0] "%1 sekundo"
msgstr[1] "%1 sekundoj"

#: src/ScreenshotDialog.qml:52
#, kde-format
msgid "Content Options"
msgstr "Enhavaj Opcioj"

#: src/ScreenshotDialog.qml:56
#, kde-format
msgid "Include cursor pointer"
msgstr "Inkluzivi kursoran montrilon"

#: src/ScreenshotDialog.qml:61
#, kde-format
msgid "Include window borders"
msgstr "Inkluzivi fenestrajn randojn"

#: src/ScreenshotDialog.qml:74
#, kde-format
msgid "Save"
msgstr "Konservi"

#: src/ScreenshotDialog.qml:84
#, kde-format
msgid "Take"
msgstr "Preni"

#: src/session.cpp:169
#, kde-format
msgctxt "@action:inmenu stops screen/window sharing"
msgid "End"
msgstr "Fini"

#: src/session.cpp:255
#, kde-format
msgctxt ""
"SNI title that indicates there's a process remotely controlling the system"
msgid "Remote Desktop"
msgstr "Fora Labortablo"

#: src/session.cpp:271
#, kde-format
msgctxt "%1 number of screens, %2 the app that receives them"
msgid "Sharing contents to %2"
msgid_plural "%1 video streams to %2"
msgstr[0] "Kundividante enhavon al %2"
msgstr[1] "%1 videofluoj al %2"

#: src/session.cpp:275
#, kde-format
msgctxt ""
"SNI title that indicates there's a process seeing our windows or screens"
msgid "Screen casting"
msgstr "Ekranelsendado"

#: src/session.cpp:445
#, kde-format
msgid ", "
msgstr ", "

#: src/userinfodialog.cpp:32
#, kde-format
msgid "Share Information"
msgstr "Kunhavigi Informojn"

#: src/userinfodialog.cpp:33
#, kde-format
msgid "Share your personal information with the requesting application?"
msgstr "Ĉu kunhavigi viajn personajn informojn kun la petanta aplikaĵo?"

#: src/waylandintegration.cpp:293 src/waylandintegration.cpp:350
#, kde-format
msgid "Failed to start screencasting"
msgstr "Malsukcesis komenci ekranelsendon"

#: src/waylandintegration.cpp:653
#, kde-format
msgid "Remote desktop"
msgstr "Fora labortablo"
